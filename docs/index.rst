Project AH2020 (workarea PPS2)
==============================

The Augmented Humanity Project (Augmanity/AH2020) is essentially based on three main challenges:

- **Emission reductions** - improvind the efficiency of industrial processes, as well as corresponding emission reductions;
- **Adequacy of production** - the development and adequacy of production processes accordiong to the characteristics of the active population;
- **Industry 4.0** - the preparation of human resources for a new industrial reality. The project presents a consortium of 15 companies and 8 R&D entities, which are coordinated by BOSCH TERMOTECNOLOGIA. It is segregated into 6 PPS (workareas) that aim to develop innovative and integrable solutions in an industrial environment.

This document is specifically related to the workarea AH2020.PPS2, which is implemented as a sBRO project.

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/b9mJrzdlfR8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
   <br/><br/>


------------

HIGHLIGHTS
----------

------------

- Portfolio of the paper [P04]_ about the use case [UC01C]_

.. image:: portfolios/AH2020.PPS2.P04.png
   :alt: Paper portfolio
   :width: 800
   :target: https://gitlab.com/sDLsystem/ah2020pps2-docs/-/raw/master/docs/portfolios/AH2020.PPS2.P04.pdf?inline=false



------------

**TABLE OF CONTENT**

------------

.. toctree::
   :maxdepth: 3
   :caption: PROJECT INFORMATION

   D00-Scheme
   D01-Scope
   D02-Funding
   D03-MissionPurpose
   D04-VisionAmbition
   D05-WorkAreas

.. toctree::
   :maxdepth: 3
   :caption: AH2020.PPS2 (WorkArea)

   AH2020.PPS2/InnovativeCharacteristics
   AH2020.PPS2/D06-Scientific-Outputs
   AH2020.PPS2/D07-WorkPackages
   AH2020.PPS2/D18-Products-Core
   AH2020.PPS2/D27-UseCases-Core
   AH2020.PPS2/D27-UseCases-Complementary


.. toctree::
   :maxdepth: 3
   :caption: sbroETL System

   AH2020.PPS2/Frameworks
   AH2020.PPS2/cdfs
   plugins/index
