--- bDinG-{inst}{NN}: **Batch datain gateway framework** (inFiles: sbroFMK-bDinG-{inst}{NN}.yml, class: GATEWAYS/project)

1. [cron bDinG-{inst}{NN}_main] Chooses the correct csv input file and inject into DinINST 

#. [...] Filter current data in DinINST and mark it

#. [...] For marked data, validate data, reject problematic data and trigger alerts

#. [...] For marked data, transform data into DinSDL (calculating the needed variables)

#. [...] For marked data, remove rows 

#. [...] Call any needed execution via "./run-sbro.sh"

#. [run] exec scheduler start

