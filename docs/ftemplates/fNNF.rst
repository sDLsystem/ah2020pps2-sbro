--- f{NN}F: **Fit {NN} framework** (inFiles: sbroFMK-f{NN}F.yml, class: DMAIC/project, class: DMAIC/project)

1. [exec sys_install/sys] (Re)create needed tables

#. [exec f{NN}F_main] Execution of the fit framework

#. [run] exec f{NN}F_main