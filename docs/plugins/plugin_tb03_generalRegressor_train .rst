.. _plugin_tb03_generalRegressor_train:

tb03_generalRegressor_train
===========================
 
:Version: 1.0
:Type: Plugin
:Developer: sBRO
:Aims: Explore some data encodings, data normalizations, feature aggregations, dimension reductions, and train several machine learning regressors as

- Bernoulli Naive Bayes
- Decision Trees 
- Extra Trees
- Gaussian Naive Bayes
- Gradient Boosting 
- K-Neighbors
- Linear Model SGDC
- Linear SVC
- Logistic Regression
- Multinomial Naive Bayes
- MLP
- Random Forest
- XGBoost


