.. _plugin_fbip09_graphs:

fbip09_graphs
=============

:Version: 1.0
:Type: Plugin
:Developer: [UA.ER]_
:Aims: Generate the bottleneck metric graphs
