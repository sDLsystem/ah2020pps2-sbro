.. _plugin_tb02_generalClassifier_predict:

tb02_generalClassifier_predict
==============================
 
:Version: 1.0
:Type: Plugin
:Developer: sBRO
:Aims: Predict the target label, using the best model trained with :ref:`plugin_tb01_generalClassifier_train`.
