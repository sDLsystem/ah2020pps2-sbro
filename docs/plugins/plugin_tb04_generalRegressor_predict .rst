.. _plugin_tb04_generalRegressor_predict:

tb04_generalRegressor_predict
=============================
 
:Version: 1.0
:Type: Plugin
:Developer: sBRO
:Aims: Predict the target variable value, using the best model trained with :ref:`plugin_tb03_generalRegressor_train`.
