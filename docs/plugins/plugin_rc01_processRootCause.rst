.. _plugin_rc01_processRootCause:

rc01_processRootCause
=====================

:Version: 1.0
:Type: Plugin
:Developer: sBRO
:Aims: Determine the contributions of process features to occurence of a target event
