.. _plugin_x05_DC_FEFFtrain:

x05_DC_FEFFtrain
================
 
:Version: 1.0
:Type: Plugin
:Developer: D. Costa (AH2020 PhD fellow / UA)
:Aims: Train some machine learning models for failure forecasting
