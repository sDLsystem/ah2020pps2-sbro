.. _plugin_x03_MJ_FBIPpredict:

x03_MJ_FBIPpredict
==================
 
:Version: 1.0
:Type: Plugin
:Developer: M.J. Lopes (BoschTT)
:Aims: Train a Random Forest model to forecast of the next bottleneck
