.. _plugin_fbip03_locstats:

fbip03_locstats
===============
 
:Version: 1.0
:Type: Plugin
:Developer: [UA.ER]_
:Aims: Calculate node bottleneck metrics (as AMPM, AQPM, and ATPM) and show some statistics
