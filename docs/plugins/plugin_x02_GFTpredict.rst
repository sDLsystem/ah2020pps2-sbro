.. _plugin_x02_GFTpredict:

x02_GFTpredict
==============
 
:Version: 1.0
:Type: Plugin
:Developer: sBRO
:Aims: From a given Generelized h-Fault Trees (GFTs) structure, fitted by :ref:`plugin_x01_GFTfit`, determines the expected occurrence time of the target event or the probability of the target event to occur at a specific instant of time.
