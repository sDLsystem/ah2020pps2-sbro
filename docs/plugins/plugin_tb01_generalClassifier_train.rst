.. _plugin_tb01_generalClassifier_train:

tb01_generalClassifier_train
============================
 
:Version: 1.0
:Type: Plugin
:Developer: sBRO
:Aims: Explore some data encodings, data normalizations, feature aggregations, dimension reductions, and train several machine learning classifiers as Bernoulli Naive Bayes, Decision Trees, Extra Trees, Gaussian Naive Bayes, Gradient Boosting, K-Neighbors, Linear Model SGDC, Linear SVC, Logistic Regression, Multinomial Naive Bayes, MLP, Random Forest, XGBoost.


