D01-Scope
=========

- **coordination**: [BTT]_

- **main_scientific_partner**: [UA]_

- **dateStart**: 01-07-2020

- **dateEnd**: 30-06-2023

- **partnersSuffix**:

.. [AAP] `AAPICO <https://www.aapico.com/>`_

.. [ALT] `Altice Labs <https://www.alticelabs.com/>`_

.. [ATN] `Atena <https://atena-ai.pt/>`_

.. [BBT] `Bosch Building Technology <https://www.bosch.pt/a-nossa-empresa/bosch-em-portugal/ovar/>`_ (in [AH2020.PPS2]_)

.. [BTT] `Bosch Thermo Technology <https://www.bosch.pt/a-nossa-empresa/bosch-em-portugal/aveiro/>`_ (in [AH2020.PPS2]_)

.. [CEN] `CENTI <https://www.centi.pt/>`_

.. [CCG] `CCG <https://ccg.pt/>`_

.. [CMF] `Critical Manufacturing <https://www.criticalmanufacturing.com/>`_

.. [EPL] `EPL <https://epl-si.com/>`_

.. [FRA] `Fraunhofer <https://www.fraunhofer.pt/>`_

.. [FCUP] `Faculdade de Ciências da Universidade do Porto <https://sigarra.up.pt/fcup/>`_ (in [AH2020.PPS2]_)

.. [FEUP] `Faculdade de Engenharia da Universidade do Porto <https://sigarra.up.pt/feup/>`_

.. [GLO] `Globatronic <https://globaltronic.pt/en/>`_

.. [GCT] `GroundControl <https://www.gcontrolgames.com/>`_

.. [HUA] `Huawei <https://www.huawei.com/en/>`_

.. [IKE] `IKEA <https://www.ikea.com/>`_

.. [IT] `Instituto de Telecomunicações <https://www.it.pt/>`_

.. [LAV] `LAVORO <https://www.lavoroeurope.com/>`_

.. [MPL] `MicroPlásticos <https://www.microplasticos.pt/>`_

.. [OLI] `Oliveira & Irmão - Sistemas Sanitários <https://www.oli-world.com/pt/>`_ (in [AH2020.PPS2]_)

.. [TIC] `TICE <https://www.tice.pt/>`_

.. [UA] `Universidade de Aveiro <https://www.ua.pt>`_ (in [AH2020.PPS2]_)

- **projectSuffix**: AH2020

- **url**: `https://www.augmanity.pt <https://www.augmanity.pt>`_

