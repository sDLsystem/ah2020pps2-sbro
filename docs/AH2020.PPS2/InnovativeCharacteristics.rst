Innovative Characteristics
==========================

.. [IC01] **Predictive maintenance in the production line of one of the most advanced water heaters in the world (UC01)**

- **Unit**: n/a

- **Market Status**: No solution exists mixing predictive maintenance, adaptive maintenance scheduling and bottleneck analysis.

- **Project Objectives**: Improve value stream by adding embedded AI maintenance devices; forecasting equipment failure; generating intelligent scheduling of maintenance times; and identifying and predicting bottlenecks.

- **Relative Significance**: 15%

- **Achivements**:

 - **Scientific**:

  - Paper [P12]_ (applied on [UA01C]_)

  - Paper [P11]_ (applied on [UA01C]_)

  - Paper [P05]_ (applied on [UC01A]_)

  - Paper [P07]_ (applied on [UC01B]_)

  - Paper [P04]_ (applied on [UC01C]_)

  - Paper [P03]_ (applied on [UC01C]_)

  - Paper [P02]_ (applied on [UC01C]_)

  - Paper [P01]_ (applied on [UC01C]_)

  - MSc thesis [T02]_ (applied on [UC01A]_)

  - On-going PhD thesis about "equipment failure forecasting" (applied on [UC01A]_, [UC04A]_)

  - On-going PhD thesis about "degradation models" (applied on [UC01B]_, [UC04B]_)

  - On-going PhD thesis about "Bottleneck and DMAIC-PDCA" (applied on [UC01C]_)

 - **Industrial**: TBD

.. [IC02] **Improvement of maintenance plans of injection molding machines (UC04)**

- **Unit**: n/a

- **Market Status**: Several solutions for preventive maintenance. Very few for predictive with embedded AI.

- **Project Objectives**: Improve maintenance plans by adding embedded AI maintenance devices; forecasting equipment failure and intelligent maintenance time scheduling.

- **Relative Significance**: 15%

- **Achivements**:

 - **Scientific**:

  - On-going paper writting (applied on [UC04A]_)

  - Paper [P15]_ (applied on [UC04B]_)

  - On-going PhD thesis about "equipment failure forecasting" (applied on [UC01A]_, [UC04A]_)

  - On-going PhD thesis about "degradation models" (applied on [UC01B]_, [UC04B]_)

 - **Industrial**: TBD

.. [IC03] **Improvement of leakage test reliability of (gas) water heaters (UC02)**

- **Unit**: n/a

- **Market Status**: Similar solutions but not for this particular problem.

- **Project Objectives**: Reduce product rejections by validating (false) rejections with external sensors and predicting product rejections.

- **Relative Significance**: 15%

- **Achivements**:

 - **Scientific**:

  - Paper [P13]_ (applied on [UA02]_)

  - Paper [P12]_ (applied on [UA02]_)

  - Paper [P09]_ (applied on [UC02]_)

  - Report [R01]_ (applied on [UC02]_)

 - **Industrial**: TBD

.. [IC04] **Improvement of quality control tests on equipment subject to external vibrations (UC03)**

- **Unit**: n/a

- **Market Status**: Some solutions but none mixing AI and root cause analysis.

- **Project Objectives**: Improve the product testing plans by determining product rejection root causes and forecasting product NOKs.

- **Relative Significance**: 15%

- **Achivements**:

 - **Scientific**:

  - Paper [P10]_ (general RC approach)

  - Paper [P06]_ (theoretical emthodology)

  - MSc thesis [T01]_ (applied on [UC03]_)

  - On-going MSc thesis about "automaic repair recomendations" (applied on [UC03]_)

  - On-going MSc thesis about "optimal re-ordering of steps in quality control tests" (applied on [UC08]_)

.. [IC05] **Frameworks for Equipment Failure Forecasting (FEFF)**

- **Unit**: n/a

- **Market Status**: We haven't found any comparable commercial solution.

- **Project Objectives**: Produce a decentralized AI solution for predictive maintenance, adapted to the Portuguese context.

- **Relative Significance**: 8%

- **Achivements**:

 - **Scientific**:

  - Paper [P05]_ (applied on [UC01A]_)

  - MSc thesis [T01]_ (applied on [UC01A]_)

  - On-going PhD thesis about "equipment failure forecast" (applied on [UC01A]_, [UC04A]_)

.. [IC06] **Frameworks for Adaptive Maintenance Time Scheduling (FAMTS)**

- **Unit**: n/a

- **Market Status**: Some solutions but with several limitations.

- **Project Objectives**: Produce an innovative preventive maintenance solution merging time based, condition based, and risk based techniques.

- **Relative Significance**: 8%

- **Achivements**:

 - **Scientific**:

  - Paper [P07]_ (applied on [UC01B]_)

  - Paper [P08]_ (applied on [UC05]_)

  - On-going PhD thesis about "degradation models" (applied on [UC01B]_, [UC04B]_)

.. [IC07] **Frameworks for Bottleneck Identification and Prediction (FBIP)**

- **Unit**: n/a

- **Market Status**: Innovative approach. No commercial solution exists.

- **Project Objectives**: Produce a reusable solution for bottleneck detection and forecasting.

- **Relative Significance**: 8%

- **Achivements**:

 - **Scientific**:

  - Paper [P04]_ (applied on [UC01C]_)

  - Paper [P03]_ (applied on [UC01C]_)

  - Paper [P02]_ (applied on [UC01C]_)

  - Paper [P01]_ (applied on [UC01C]_)

.. [IC08] **Frameworks for Product Rejection Validation and Forecasting (FPRVF)**

- **Unit**: n/a

- **Market Status**: We haven't found any comparable commercial solution.

- **Project Objectives**: Produce a reusable solution for improving quality control process by rejection validation and forecasting.

- **Relative Significance**: 8%

- **Achivements**:

 - **Scientific**:

  - Paper [P09]_ (applied on [UC02]_)

.. [IC09] **Frameworks for NOK Prioritization and Root Cause (FNPRC)**

- **Unit**: n/a

- **Market Status**: Innovative approach. No commercial solution exists.

- **Project Objectives**: Produce a root cause system merging new techniques in Physics complex networks with telecommunication network techniques.

- **Relative Significance**: 8%

- **Achivements**:

 - **Scientific**:

  - Paper [P11]_ (applied on [UC01]_)

  - Paper [P10]_ (general RC approach)

  - Paper [P06]_ (theoretical methodology)

  - MSc thesis [T01]_ (applied on [UC03]_)

  - On-going MSc thesis about "optimal re-ordering of steps in quality control tests" (applied on [UC08]_)

  - On-going MSc thesis about "image classification of combustion cameras" (applied on [UC06]_)

  - On-going PhD thesis about "root cause analysis by network methods" (applied on [UC03]_)

