.. _FNPRC_sys:

FNPRC_sys (cdf)
==============

:version: 1.0
:framework class: :ref:`fnprc`
:input data format: ---
:data processing: internal analytics database
:data output: internal analytics database
:deploy format: microservice
:scientific team: ---
:conception team: [UA.ER]_

1. Input Data
~~~~~~~~~~~~~

Configuration files.

2. Output Data
~~~~~~~~~~~~~~

Manage the generation of all SQL tables and documentation outputs.

.. [FNPRC_DinSDL] The DinSDL table for the [FNPRC]_ Product-UseCase

=======================  ====================  =================
  Field                    Type                  Observations
=======================  ====================  =================
  dt                       datetime              [1]
  dtint                    bigint                [1]
  locationID               varchar(100)          [2]
  d001                     varchar(100)          [3]
  ---                                            [3]
  dxxx                     varchar(100)          [3]
  v001                     float                 [4]
  ---                                            [4]
  vxxx                     float                 [4]
  tags                     varchar(300)          [5] 
=======================  ====================  =================

Table Observations:

- [1]: Datetime string with the format "yyyy/mm/dd HH:MM:SS.f" and its corresponding UTC integer

- [2]: The location identification of the UseCase


- [3]: The set of categorical variables (e.g. scenario variables, aggregation variables); by default are NULL

- [4]: The set of all numeric variables (e.g. sensor variables, process variables, KPIs, calculated variables); by default are NULL

- [5]: A string with space separated tags or the empty string

General Observations:

- Data is injected via :ref:`FNPRC_bDinG-BTT01`.

- The number of dxxx variables is defined in prjVG key "config/d_size" and the number of vxxx variables is defined in prjVG key "config/v_size".

1. sbroETL information
~~~~~~~~~~~~~~~~~~~~~~

.. include:: ../../../ftemplates/sys.rst

4. Components
~~~~~~~~~~~~~

------------

:Identification: sys_install
:Description: (Re)create default system tables 
:Implementation team: [UA.ER]_
:Plugins: Do not use plugins

------------

:Identification: sys_config
:Description: Set system configurations 
:Implementation team: [UA.ER]_
:Plugins: Do not use plugins

------------

:Identification: sys_docs
:Description: Generate Product-UseCase documentation  
:Implementation team: [UA.ER]_
:Plugins: Do not use plugins

------------

:Identification: sys_main
:Description: Execution of Product-UseCase maintenance tasks and management of services  
:Implementation team: [UA.ER]_
:Plugins: Do not use plugins

------------
