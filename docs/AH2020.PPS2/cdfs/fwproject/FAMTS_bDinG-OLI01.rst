.. _FAMTS_bDinG-OLI01:

FAMTS_bDinG-OLI01 (cdf)
=======================

:version: 0.9
:framework class: :ref:`famts`
:input data format: batch / csv file
:data injection technique: cron file watch
:data processing: internal analytics database
:data output: internal analytics database
:deploy format: microservice
:scientific team: --
:conception team: [OLI.RA]_ [UA.BM]_ [UA.ER]_ [UA.PN]_


1. Input Data
~~~~~~~~~~~~~

Extract data from Injection Mold Machines (see [UC04B]_), using the EuroMap protocol, and inject the data to the internal analytics database.

.. [bDinG-OLI01-DinINST] Input data table for Injection Mold Machines at [OLI]_ for [FAMTS]_. 

(Information will be added)

1. Output Data
~~~~~~~~~~~~~~

Output data is kept in the internal analytics database.

3. sbroETL information
~~~~~~~~~~~~~~~~~~~~~~

.. include:: ../../../ftemplates/bDinG.rst

4. Components
~~~~~~~~~~~~~

------------

:Identification: bDinG-OLI01_INJ01
:Description: Watch a directory for a new csv file and inject it to the internal analytics database in a normalized format.
:Implementation team: [UA.BM]_ [UA.PN]_ [UA.ER]_
:Plugins: Do not use plugins.

------------
