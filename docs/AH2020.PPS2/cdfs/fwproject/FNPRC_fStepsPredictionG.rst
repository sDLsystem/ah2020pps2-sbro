.. _FNPRC_fStepsPredictionG:

FNPRC_fStepsPredictionG (cdf)
=============================

:version: 0.9
:framework class: :ref:`fnprc`
:input data format: internal analytics database
:data processing: internal analytics database
:data output: internal analytics database
:deploy format: microservice
:scientific team: [UA.ER]_
:conception team: [UA.MP]_ [UA.ER]_ [BBT.AL]_

1. Input Data
~~~~~~~~~~~~~

Input data is obtained from the internal analytics database.

2. Output Data
~~~~~~~~~~~~~~

Output data is kept in the internal analytics database to be used by the framework :ref:`FNPRC_maStepsPredictionG`.

3. sbroETL information
~~~~~~~~~~~~~~~~~~~~~~

.. include:: ../../../ftemplates/fNNF.rst

4. Components
~~~~~~~~~~~~~

------------

:Identification: fStepsPredictionG_TRAIN01
:Description: Train several classifiers for the forecast step NOKs
:Implementation team: [UA.ER]_
:Plugins: The following are the plugins used

- :ref:`plugin_tb01_generalClassifier_train`

------------
