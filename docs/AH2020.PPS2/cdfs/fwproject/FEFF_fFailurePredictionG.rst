.. _FEFF_fFailurePredictionG:

FEFF_fFailurePredictionG (cdf)
==============================

:version: 0.9
:framework class: :ref:`feff`
:input data format: internal analytics database
:data processing: internal analytics database
:data output: internal analytics database
:deploy format: microservice
:scientific team: [UA.ER]_ [UA.JPS]_
:conception team: [UA.DC]_ [UA.ER]_ [UA.DC]_ [BTT.DA]_ [BTT.PR]_

1. Input Data
~~~~~~~~~~~~~

Input data is obtained from table [FEFF_DinSDL]_.

2. Output Data
~~~~~~~~~~~~~~

Output data is kept in the internal analytics database to be used by the framework :ref:`FEFF_maFailurePredictionG`.

3. sbroETL information
~~~~~~~~~~~~~~~~~~~~~~

.. include:: ../../../ftemplates/fNNF.rst

4. Components
~~~~~~~~~~~~~

------------

:Identification: fFailurePredictionG_ANN01
:Description: Find the last (reduced) windowed anomalies by COPOD
:Implementation team: [UA.DC]_ [UA.ER]_
:Plugins: The following are the plugins used

- :ref:`plugin_x04_DC1_FEFFann`

------------

:Identification: fFailurePredictionG_TRAIN01
:Description: Train some machine learning models for failure forecasting
:Implementation team: [UA.DC]_
:Plugins: The following are the plugins used

- :ref:`plugin_x05_DC_FEFFtrain`

------------

:Identification: fFailurePredictionG_TRAIN02
:Description: Train some machine learning models for failure forecasting
:Implementation team: [UA.ER]_
:Plugins: The following are the plugins used

- :ref:`plugin_tb01_generalClassifier_train`

------------
