.. _FNPRC_maStepsPredictionG:

FNPRC_maStepsPredictionG (cdf)
==============================

:version: 0.9
:framework class: :ref:`fnprc`
:input data format: internal analytics database
:data processing: internal analytics database
:data output: internal analytics database
:deploy format: microservice
:scientific team: [UA.ER]_
:conception team: [UA.ER]_ [UA.MP]_ [BBT.AL]_

1. Input Data
~~~~~~~~~~~~~

Input data is obtained from the internal analytics database.

2. Output Data
~~~~~~~~~~~~~~

Results are kept in the internal analytics database, shared in PDF reports, and available for other modules.

3. sbroETL information
~~~~~~~~~~~~~~~~~~~~~~

.. include:: ../../../ftemplates/maNNF.rst

4. Components
~~~~~~~~~~~~~

------------

:Identification: fStepsPredictionG_PRED01
:Description: Forecast step NOKs
:Implementation team: [UA.ER]_
:Plugins: The following are the plugins used

- :ref:`plugin_tb02_generalClassifier_predict`

------------
