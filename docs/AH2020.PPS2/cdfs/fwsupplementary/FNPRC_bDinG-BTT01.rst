.. _FNPRC_bDinG-BTT01:

FNPRC_bDinG-BTT01 (cdf)
=======================

:version: 0.9
:framework class: :ref:`fnprc`
:input data format: batch / csv file
:data injection technique: cron file watch
:data processing: internal analytics database
:data output: internal analytics database
:deploy format: microservice
:scientific team: --
:conception team: [BBT.PR]_ [UA.ER]_


1. Input Data
~~~~~~~~~~~~~

Input data comes from csv files exported from the Nexeed MES.


2. Output Data
~~~~~~~~~~~~~~

Output data is kept in the internal analytics database.

3. sbroETL information
~~~~~~~~~~~~~~~~~~~~~~

.. include:: ../../../ftemplates/bDinG.rst

4. Components
~~~~~~~~~~~~~

------------

:Identification: bDinG-BTT01_INJ01
:Description: Watch a directory for a new csv file and inject it to the internal analytics database in a normalized format.
:Implementation team: [UA.ER]_
:Plugins: Do not use plugins.

------------
