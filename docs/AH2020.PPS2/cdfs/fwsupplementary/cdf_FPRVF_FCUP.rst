.. _FPRVF_FCUP:

FPRVF_FCUP (cdf)
================

:version: 0.9
:framework class: :ref:`fprvf`
:input data format: internal analytics database
:data processing: internal analytics database
:data output: internal analytics database
:deploy format: microservice
:scientific team: Inês Dutra (FCUP), F. Rocha (AH2020 PhD fellow / FCUP)
:conception team: Inês Dutra (FCUP), F. Rocha (AH2020 PhD fellow / FCUP) 

1. Description
~~~~~~~~~~~~~~

(Information will be added, when available)

2. Input Data
~~~~~~~~~~~~~~

(Information will be added, when available)


3. Output Data
~~~~~~~~~~~~~~

(Information will be added, when available)


4. Components
~~~~~~~~~~~~~

------------

:Identification: RUN01
:Description: (Information will be added, when available)
:Implementation team: Inês Dutra (FCUP), F. Rocha (AH2020 PhD fellow / FCUP)
:Plugins: The following are the plugins used

- :ref:`plugin_FPRVF_FCUP`

------------
