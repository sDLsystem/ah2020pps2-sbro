D27-UseCases-Core
=================

------------

UC01A
~~~~~

------------

.. [UC01A] **AH2020.PPS2.U01A - Smart Press (equipment failure forecasting)**

- **aims**: The aim is to reduce costs on the value stream of FP Low Nox (Greentherm T9000 series), one of the most advanced water heaters (WH) in the world, directed to the USA and Canada markets; by (a) adding some sensor edge devices with embedded AI maintenance algorithms and forecasting equipment failure; (b) generating better scheduling of equipment maintenance operations; and (c) identifying and predicting bottlenecks. The FP Low Nox annual production is around 15000 HR, where its mounting involves 18 cells in the lines S843, S842, S851 and L8; all producing a huge amount of data. Although this product will be the basis for the use case and frameworks validation and testing, we intend to explore the possibility of replicating the approach to other product value streams during activity A13. The expected improvements will not only reduce costs and increase productivity but indirectly impact on the operators work scheduling and the water heater brand recognition worldwide. This use case focus on the item (a) for smart presses.

- **copromotors**: [BTT]_ [UA]_

- **location**: BTT (SmartPress SP1)

- **state**: Algorithm development

- **teams**:

 - **T00**: [BTT.DA]_ [UA.ER]_ [CMF.RM]_

 - **T01**: [UA.ER]_

 - **T02**: [UA.ER]_

 - **T03**: [UA.DC]_ [UA.ER]_ [BTT.DA]_ [BTT.PR]_ [UA.JPS]_

 - **T05**: [BTT.DA]_ [UA.ER]_ [CMF.RM]_

- **product**: [AH2020.PPS2.M.FEFF]_

- **current_attained_results**: TBD

------------

UC01B
~~~~~

------------

.. [UC01B] **AH2020.PPS2.U01B - Smart Press (degradation models)**

- **aims**: To reduce costs on the value stream of FP Low Nox (Greentherm T9000 series), one of the most advanced water heaters (WH) in the world, directed to the USA and Canada markets; by (a) adding some sensor edge devices with embedded AI maintenance algorithms and forecasting equipment failure; (b) generating better scheduling of equipment maintenance operations; and (c) identifying and predicting bottlenecks. The FP Low Nox annual production is around 15000 HR, where its mounting involves 18 cells in the lines S843, S842, S851 and L8; all producing a huge amount of data. Although this product will be the basis for the use case and frameworks validation and testing, we intend to explore the possibility of replicating the approach to other product value streams during activity A13. The expected improvements will not only reduce costs and increase productivity but indirectly impact on the operators work scheduling and the water heater brand recognition worldwide. This use case focus on the item (a) for smart presses.

- **copromotors**: [BTT]_ [UA]_

- **location**: BTT (SmartPress SP1)

- **state**: Algorithm development

- **teams**:

 - **T00**: [BTT.DA]_ [UA.ER]_ [CMF.RM]_

 - **T01**: [UA.ER]_

 - **T02**: [UA.ER]_

 - **T03**: [UA.PN]_ [UA.ER]_ [BTT.DA]_ [BTT.PR]_ [UA.JPS]_

 - **T05**: [BTT.DA]_ [UA.ER]_ [CMF.RM]_

- **product**: [AH2020.PPS2.M.FAMTS]_

- **current_attained_results**: TBD

------------

UC01C
~~~~~

------------

.. [UC01C] **AH2020.PPS2.U01C - Bottleneck on L7**

- **aims**: To reduce costs on the value stream of FP Low Nox (Greentherm T9000 series), one of the most advanced water heaters (WH) in the world, directed to the USA and Canada markets; by (a) adding some sensor edge devices with embedded AI maintenance algorithms and forecasting equipment failure; (b) generating better scheduling of equipment maintenance operations; and (c) identifying and predicting bottlenecks. The FP Low Nox annual production is around 15000 HR, where its mounting involves 18 cells in the lines S843, S842, S851 and L8; all producing a huge amount of data. Although this product will be the basis for the use case and frameworks validation and testing, we intend to explore the possibility of replicating the approach to other product value streams during activity A13. The expected improvements will not only reduce costs and increase productivity but indirectly impact on the operators work scheduling and the water heater brand recognition worldwide. This use case focus on the item (c) in the line cell L10.

- **copromotors**: [BTT]_ [UA]_

- **location**: [BTT]_ (line cell L7)

- **state**: Algorithm development

- **teams**:

 - **T00**: [BTT.DA]_ [UA.ER]_ [CMF.RM]_

 - **T01**: [UA.ER]_

 - **T02**: [UA.ER]_

 - **T03**: [UA.AB]_ [UA.ER]_ [UA.AM]_ [UA.AS]_ [BTT.DA]_ [BTT.PR]_

 - **T04**: [BTT.MJL]_ [UA.ER]_

 - **T05**: [BTT.DA]_ [UA.ER]_ [CMF.RM]_

- **product**: [AH2020.PPS2.B.FBIP]_

- **current_attained_results**: TBD

------------

UC02
~~~~

------------

.. [UC02] **AH2020.PPS2.U02 - Leakage Reliability Tests**

- **aims**: To improve leakage reliability tests via data correlation with ambient measures. Bosch TT produced more than 250000 water heaters in 2018, where the majority is of gas combustion type. Gas leak detection is of key importance because of safety, liability, and health concerns, and European regulations and warranties. For operation purposes, these equipment tests need to be very sensitive. Although this sensitiveness is controlled, equipment misbehavior can happen in situations that do not actually represent an error/fault; related to changes in ambient temperature, vibration, bad localization or even usage time; somehow related with its high precision. Such bad behavior usually produces false product rejections. Thus, a solution should address two issues: (a) validate (false) rejections by adding extra information (sensoring); (b) classify and forecast rejections, using machine learning techniques. The products under study have a ratio of 60% OK / 40% NOKs, showing how costly the problem is and its potential for improvement.

- **copromotors**: [BTT]_ [FCUP]_ [UA]_

- **location**: [BTT]_ (?)

- **state**: Algorithm development

- **teams**:

 - **T00**: [BTT.DA]_ [UA.ER]_ [CMF.RM]_

 - **T01**: [UA.ER]_

 - **T02**: [UA.ER]_

 - **T03**: [UA.DC]_ [UA.ER]_ [BTT.DA]_ [BTT.PR]_ [UA.JPS]_

 - **T04**: [FCUP.ID]_ [FCUP.FR]_ [BTT.DA]_ [BTT.PR]_

 - **T05**: [BTT.DA]_ [UA.ER]_ [CMF.RM]_

- **product**: [AH2020.PPS2.Q.FPRVF]_

- **current_attained_results**: TBD

------------

UC03
~~~~

------------

.. [UC03] **AH2020.PPS2.U03 - Quality Tests of PCBAs**

- **aims**: To improve the reliability of quality control tests made on equipment subject to external vibrations by predicting product rejections and using innovative techniques for root cause analysis. Besides forecasting and root cause analysis, the methodologies will be able to produce a set of recommendations regarding the best order of steps and identifying redundant steps from a given sequence of test steps. The machines are at the Manufacturing Defect Analyzer on line 28 and process approximately 1300 Printed Circuit Board Assembly (PCBA) per day with around 400 measurements per PCBA (e.g. resistance, capacity, inductance, shorts). The production forecast for 2019 is of 269K pieces, where the main product tested are water heater controllers with approximately 10% false rejections and implying 4h/month of downtime for calibration issues. We intend to explore the possibility of replicating the approach to other testing equipment during activity A13.

- **copromotors**: [BBT]_ [UA]_

- **location**: [BBT]_

- **state**: Algorithm development

- **teams**:

 - **T00**: [BBT.AL]_ [UA.ER]_ [CMF.RM]_

 - **T01**: [UA.ER]_

 - **T02**: [UA.ER]_

 - **T03**: [UA.FB]_ [UA.GB]_ [UA.DG]_ [BBT.AL]_

 - **T04**: [UA.MP]_ [UA.PC]_ [UA.ER]_ [BBT.AL]_

 - **T05**: [BBT.AL]_ [UA.ER]_ [CMF.RM]_

- **product**: [AH2020.PPS2.Q.FNPRCI]_

- **current_attained_results**: TBD

------------

UC04A
~~~~~

------------

.. [UC04A] **AH2020.PPS2.U04A - Injection Molding Machines (forecasting equipment failure)**

- **aims**: To improve maintenance plans and reduce costs of injection molding machines by predicting equipment degradation and adjusting the schedule of maintenance operations. The factory has 90 injection molding machines, more than 1283 molds, 50 robots and 87 assembly work stations. Injection molding machines, devices and molds generate huge amounts of data, and each machine has dozens of sensors to measure temperature, flow rates, pressures, and, the newer the machine the more data it generates. However, the data is not being properly exploit. Managing this extensive range of machines and molds is extremely complex and entails very high repair and maintenance costs. Unscheduled mold and/or machine stops can occur for many different reasons like Mold Damage, Heater Issues, Electrical Issues, Incorrect Mold Assembly, Internal Water Leak, Ejector Plate Won't Function, External Oil Leak, Broken Gate, Scuffed Tooling, among others. The current approach for maintenance is mainly corrective and preventive maintenance which is not efficient, e.g., the downtime implies labor and work station stops with costs of several hundred thousand euros, but the tooling costs and external services can be two times higher.

- **copromotors**: [FCUP]_ [BTT]_ [UA]_

- **location**: [BTT]_

- **state**: Data analysis

- **teams**:

 - **T00**: [OLI.RA]_ [UA.ER]_ [CMF.RM]_

 - **T01**: [UA.ER]_

 - **T02**: [UA.ER]_

 - **T03**: [UA.DC]_ [UA.ER]_ [OLI.RA]_ [UA.JPS]_

 - **T05**: [OLI.RA]_ [UA.ER]_ [CMF.RM]_

- **product**: [AH2020.PPS2.M.FEFF]_

- **current_attained_results**: TBD

------------

UC04B
~~~~~

------------

.. [UC04B] **AH2020.PPS2.U04B - Injection Molding Machines (degradation models)**

- **aims**: To improve maintenance plans and reduce costs of injection molding machines by predicting equipment degradation and adjusting the schedule of maintenance operations. The factory has 90 injection molding machines, more than 1283 molds, 50 robots and 87 assembly work stations. Injection molding machines, devices and molds generate huge amounts of data, and each machine has dozens of sensors to measure temperature, flow rates, pressures, and, the newer the machine the more data it generates. However, the data is not being properly exploit. Managing this extensive range of machines and molds is extremely complex and entails very high repair and maintenance costs. Unscheduled mold and/or machine stops can occur for many different reasons like Mold Damage, Heater Issues, Electrical Issues, Incorrect Mold Assembly, Internal Water Leak, Ejector Plate Won't Function, External Oil Leak, Broken Gate, Scuffed Tooling, among others. The current approach for maintenance is mainly corrective and preventive maintenance which is not efficient, e.g., the downtime implies labor and work station stops with costs of several hundred thousand euros, but the tooling costs and external services can be two times higher.

- **copromotors**: [BTT]_ [UA]_

- **location**: [BTT]_

- **state**: Data analysis

- **teams**:

 - **T00**: [OLI.RA]_ [UA.ER]_ [CMF.RM]_

 - **T01**: [UA.ER]_

 - **T02**: [UA.ER]_

 - **T03**: [UA.PN]_ [UA.ER]_ [OLI.RA]_ [UA.JPS]_

 - **T05**: [OLI.RA]_ [UA.ER]_ [CMF.RM]_

- **product**: [AH2020.PPS2.M.FAMTS]_

- **current_attained_results**: TBD

