D27-UseCases-Complementary
==========================

------------

UC05
~~~~

------------

.. [UC05] **AH2020.PPS2.U05 - Friction Welding**

- **aims**: TBD

- **copromotors**: [BTT]_ [UA]_

- **location**: [BTT]_

- **state**: Results validation

- **teams**:

 - **T00**: [BTT.PR]_ [UA.ER]_ [CMF.RM]_

 - **T01**: [UA.ER]_

 - **T02**: [UA.ER]_

 - **T03**: [UA.PN]_ [UA.ER]_ [BTT.PR]_

 - **T05**: [BTT.PR]_ [UA.ER]_ [CMF.RM]_

- **product**: [AH2020.PPS2.M.FAMTS]_

- **current_attained_results**: TBD

------------

UC06
~~~~

------------

.. [UC06] **AH2020.PPS2.U06 - Combustion detection**

- **aims**: TBD

- **copromotors**: [BTT]_ [UA]_

- **location**: [BTT]_

- **state**: Hardware deployment and algorithm testing

- **teams**:

 - **T01**: [UA.RP]_ [BTT.DP]_ [UA.ER]_ [UA.JPS]_

- **product**: [TBD]_

- **current_attained_results**: TBD

------------

UC07
~~~~

------------

.. [UC07] **AH2020.PPS2.U07 - KPI evaluation, root cause and continuous improvement methodologies**

- **aims**: TBD

- **copromotors**: [BTT]_ [UA]_

- **location**: [BTT]_ Logistics Department

- **state**: Algorithm testing

- **teams**:

 - **T01**: [BTT.MJL]_ [UA.ER]_

 - **T02**: [UA.AB]_ [UA.ER]_ [UA.CP]_

- **product**: [TBD]_

- **current_attained_results**: TBD

------------

UC08
~~~~

------------

.. [UC08] **AH2020.PPS2.U08 - Optimal re-ordering of steps in quality control tests**

- **aims**: TBD

- **copromotors**: [BTT]_ [UA]_

- **location**: [BTT]_ Line 7

- **state**: Algorithm testing

- **teams**:

 - **T01**: [UA.DM]_ [UA.ER]_ [BTT.PR]_

- **product**: [TBD]_

- **current_attained_results**: TBD

