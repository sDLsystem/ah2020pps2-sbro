D18-Products-Core
=================

------------

AH2020.PPS2.M.FEFF
~~~~~~~~~~~~~~~~~~

------------

.. [AH2020.PPS2.M.FEFF] **Frameworks for equipment failure forecasting**

- **shortDescription**: Adapted to the specific features of the Portuguese industrial situation and to effectively address the associated UseCases, we propose a decentralized solution for predictive maintenance, on the deploying mode part of AAFM, merged with a centralized solution on the calibration and training mode part of AAFM. The decentralized solution will have embedded predictive maintenance algorithms in edge devices (e.g. XDK/Bosch IOTiP/Fraunhofer14, or DEMA/UA), allowing an effective acquisition and preprocessing of maintenance data to be globally processed by the CTM part, e.g. using machine learning techniques (see D.4). FEEF will detect and predict anomalies and failure/degradation patterns, providing early warnings and as a result allowing to adapt maintenance plans in advance. This may result in several cost savings by avoiding or minimizing the downtimes or reducing the frequency of maintenance (i.e. optimize maintenance operations). Although not new, predictive maintenance still has a growing relevance in modern industry. The existing centralized architectures for this type of problem are based on the application of predictive maintenance techniques in the cloud (central element), thus the entire process is dependent on a single element, and a failure of the communication between machine and central can inhibit an alarm which will cause possible critical equipment failure. Furthermore, existing decentralized architectures do not have the ability to predict the time window in which the failure will occur, so there is still no decentralized global solution that is effectively efficient in real time, without also merging it with a centralized global approach; which means that an orchestration of both solutions is required. Furthermore, each edge product with embedded algorithms (at the DM level of AAFM) will provide SOAP/REST services allowing easier integration into any manufacturing environment and communication between shop-floor equipment.

- **D19-EstimatedGains&Costs**: TBD

- **D20-Technology**:

 - **coordinator**: [UA.ER]_

 - **producers**: [UA]_

 - **consumers**: [CMF]_

- **D21-ScientificCoordination**: [UA.ER]_ [UA.JPS]_

- **D22-DeploymentCoordination**: [CMF.RM]_ [BTT.DA]_ [OLI.RA]_ [UA.ER]_

- **D23-Features**: TBD

- **D24-KPIdesc**:

 - **k000**: minimize | Cost of the number of stops in a BlockTime | python_formulae using DaggSDL

 - **k001**: minimize | Cost of the total duration of stops in a BlockTime | python_formulae using DaggSDL

------------

AH2020.PPS2.M.FAMTS
~~~~~~~~~~~~~~~~~~~

------------

.. [AH2020.PPS2.M.FAMTS] **Frameworks for adaptive maintenance time scheduling**

- **shortDescription**: To improve the existing preventive maintenance models, we propose an innovative framework which merges methodologies from time based maintenance, risk maintenance, condition based maintenance, and predictive maintenance; by generating maintenance times obtained from applying techniques of risk analysis, logic theory and machine learning. By far the most common approach in the Portuguese industry context is time based maintenance (TBM), which assumes that equipment service life and failure events can be determined by age, relying in the premise that TBM is (reasonably) cost effective when compared to methods with higher effort of assessment accuracy. However the majority of failure modes in a plant are not of TBM type. In FAMTS, to optimize the maintenance program we use time series forecasting techniques and, taking into account the inherent uncertainty associated with the occurrence of failures, merge it with algorithms based on the use of risk measures, such as the conditional value at risk. Those risk measures will also consider equipment failure forecasts (see FEFF), when available. Although risk measures were initially developed to finance problems, their use rapidly spread to other management problems17 since they allow to measure, and consequently control, the occurrence of undesirable events provided that the distributions of the uncertain parameters is known. For the conditional based approach and when dealing with the discrete, nominal data, we intend to use the team expertise in Inductive Logic Programming (ILP) to extract interpretable rules from the data. Because ILP can handle relational data, other kinds of rules can be generated such as: “equipment with certain given characteristics that are near to other equipment/devices which have a given behavior are more likely to produce a bad behavior or a false bad behavior”. These rules, that would be continuously inferred from the information gathered, will be also used to build an evolutive checkable model18 on a suitable Dynamic Logic, that will pave the way for a continuous and iterative maintenance analysis, in two ways. First, by supporting periodic checks of a set of obligation properties, in order to trigger valuable maintenance warnings; and secondly, by offering to the user the opportunity of checking directly complex execution scenarios and event combinations, using dynamic logics expressions. The combination of these techniques seems to be a significant push in maintenance time scheduling approaches.

- **D19-EstimatedGains&Costs**: TBD

- **D20-Technology**:

 - **coordinator**: [UA.ER]_

 - **producers**: [UA]_

 - **consumers**: [CMF]_

- **D21-ScientificCoordination**: [UA.ER]_

- **D22-DeploymentCoordination**: [CMF.RM]_ [BTT.DA]_ [OLI.RA]_ [UA.ER]_

- **D23-Features**: TBD

- **D24-KPIdesc**:

 - **k000**: minimize | Cost of TBD in a BlockTime

 - **k001**: minimize | Cost of TBD in a BlockTime

------------

AH2020.PPS2.B.FBIP
~~~~~~~~~~~~~~~~~~

------------

.. [AH2020.PPS2.B.FBIP] **Frameworks for bottleneck identification and prediction**

- **shortDescription**: In industrial manufacturing environments, characterized as being stochastic, dynamic and often chaotic, disturbance handling is a crucial issue in the development of intelligent and reconfigurable manufacturing control systems. The occurrence of expected or unexpected bottlenecks, due to the cadence of some equipment or work stations, lead to deviations in the production plans. Usually it degrades the performance of the system, causing the loss of productivity and business opportunities, which are crucial roles to achieve competitiveness. Traditional manufacturing systems are not prepared to exhibit responsiveness and reconfigurability capabilities, since they are built upon centralized and hierarchical control structures. Those systems present good production optimization but a weak response to change due to the rigidity and centralization of their control structures, usually requiring the shutdown of the partial or even whole system when a bottleneck occurs. Hence the challenge faced by the manufacturing companies, to remain competitive, copes with the need to implement manufacturing control systems that exhibit innovative features, as agile response to the occurrence of the bottlenecks and dynamic re-configuration without stopping, re-programming or re-starting the process. A decision support system appears as suitable emergent paradigms to address this challenge, suggesting the definition of distributing control based on autonomous and cooperative units that account for the realization of efficient, flexible and robust overall plant control. However, a complete and integrated approach for bottlenecks analysis, that is able to detect, predict, diagnosis, replanning and prognosis the major types of shop floor bottlenecks with impact at planning and scheduling level, is still missing. The proposed bottleneck detection and handling decision support system, introduces the following innovative aspects: (a) automatic detection of bottlenecks; (b) differentiation between typical and atypical bottlenecks; (c) besides the bottlenecks detection and classification, also considers other kind of shop floor disturbances, such as delays and rush orders, that may have an impact at planning and scheduling level; (d) considers a distributed and reactive approach to the re-scheduling problem, executed as fast as possible to avoid the risk of degradation of the production activity; (e) integrates a prognosis component, allowing planning in advance the future occurrence of bottlenecks, transforming the traditional “fail and recovering” into “predict and prevent” practices. Furthermore, predictive analysis will be also pursued using machine learning (more details in E.2). The expected results will contribute to help industrial companies to improve their competitiveness, namely through mechanisms and products that will enable these companies to be highly responsive to bottlenecks occurrences.

- **D19-EstimatedGains&Costs**: TBD

- **D20-Technology**:

 - **coordinator**: [UA.ER]_

 - **producers**: [UA]_

 - **consumers**: [CMF]_

- **D21-ScientificCoordination**: [UA.ER]_ [UA.AM]_

- **D22-DeploymentCoordination**: [CMF.RM]_ [BTT.DA]_ [UA.ER]_

- **D23-Features**: TBD

- **D24-KPIdesc**:

 - **k000**: minimize | Cost of TBD in a BlockTime

 - **k001**: minimize | Cost of TBD in a BlockTime

------------

AH2020.PPS2.Q.FPRVF
~~~~~~~~~~~~~~~~~~~

------------

.. [AH2020.PPS2.Q.FPRVF] **Frameworks for partnumber rejection validation and forecasting**

- **shortDescription**: Technology allowed to build devices/equipment that are quite reliable, but at the same time, very sensitive. Although this sensitiveness is controlled, equipment bad behavior can happen in situations that do not actually represent it. Factors such as changes in ambient temperature, vibration, bad localization or usage time could affect the quality of their responses. Such bad behavior on quality control equipment usually reflects on product NOKs or (false) rejections, being some rejections induced by the test process itself. An advantage today is that large amounts of data related to devices/equipment functioning, their surroundings and environment, and product characteristics, are collected on a daily/hourly/minute/second basis and stored in databases, clouds or as simple files in hard disks. This data becomes a very rich source of information that can allow: (1) speedy reactions to simple situations (like replacement of an old or faulty device) and to extreme situations (for example, prediction of false rejections before they happen), (2) emission of timely alerts, or (3) prediction of non-optimal or hazard situations. This data can also be used to build specialized behavior and prediction models according to the kind of product tests done. We can use the behavior models to assess differences among test machines of the same type, placed in different locations or placed in the same location, in the same pipeline or in distinct pipelines. We could also assess differences between test equipment of different types. Studies about these behaviors could be useful to plan future installations, where one wants to minimize false bad behaviors and general costs. Moreover, predicting NOKs allow to introduce corrective measures on the test plans or even correct design issues in test equipment.

- **D19-EstimatedGains&Costs**: TBD

- **D20-Technology**:

 - **coordinator**: [UA.ER]_

 - **producers**: [UA]_

 - **consumers**: [CMF]_

- **D21-ScientificCoordination**: [UA.ER]_ [FCUP.ID]_

- **D22-DeploymentCoordination**: [CMF.RM]_ [BBT.AL]_ [BTT.DA]_ [UA.ER]_

- **D23-Features**: TBD

- **D24-KPIdesc**:

 - **k000**: minimize | Cost of TBD in a BlockTime

 - **k001**: minimize | Cost of TBD in a BlockTime

------------

AH2020.PPS2.Q.FNPRCI
~~~~~~~~~~~~~~~~~~~~

------------

.. [AH2020.PPS2.Q.FNPRCI] **Frameworks for NOK prioritization and root cause identification**

- **shortDescription**: We propose a dependency network approach to the problem of minimizing errors in production and quickly identifying the root cause of such problems (e.g. NOK, failures). Complex Network Theory will be applied to approach quality control. This approach is flexible, scalable and gives good estimates with a reasonable computational overhead. Industrial products usually result from the integration of many components, in many production steps, from numerous raw materials and suppliers, involving various equipment and personal. The idea is to represent the production process as a directed network. Nodes in that network represent steps in the process, operators or components, and edges represent dependency or influence relationships. The root causes of an anomaly or failure may then be identified using graph theory methods. In particular, factory maintenance teams gather information through documents detailing events, identifying involved systems and documenting solutions. This constitutes a basis of an Expert System which can be data mined in order to produce a dependency network. Such a dependency network approach has been applied to the root cause analysis problem in telecommunication systems, software development, infrastructure supply networks, among others. To the best of our knowledge, it has not been applied to factory management systems. The main discussed issue arises when a small number of original failures cascade through such a network, leading to large set of failures. The problem then becomes identifying the root cause of a failure, given the final state and the dependency network. The exact solution is an NP-complete problem. A heuristic approach to the closely related problem of finding the minimal set to damage the whole network, iteratively calculating weights for nodes in the network converges rapidly, and gives excellent results compared with other computationally intensive methods. This framework will significantly facilitate the work of maintenance teams, in scenarios with complex relations and big datasets of information.

- **D19-EstimatedGains&Costs**: TBD

- **D20-Technology**:

 - **coordinator**: [UA.ER]_

 - **producers**: [UA]_

 - **consumers**: [CMF]_

- **D21-ScientificCoordination**: [UA.ER]_ [UA.GB]_

- **D22-DeploymentCoordination**: [CMF.RM]_ [BBT.AL]_ [BTT.PR]_ [UA.ER]_

- **D23-Features**: TBD

- **D24-KPIdesc**:

 - **k000**: minimize | Cost of TBD in a BlockTime

 - **k001**: minimize | Cost of TBD in a BlockTime

