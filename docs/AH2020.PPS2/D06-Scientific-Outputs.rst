D06-Scientific-Outputs
======================

**Statistics for AH2020.PPS2**
 
===============  ==========  ========== ========= ========== ============
Type             Produced    Published  Accepted  Submitted  Distintions 
===============  ==========  ========== ========= ========== ============
Papers           15          4          8         3          ---
Reports/Posters  3           ---        ---       ---        ---
Communications   10          ---        ---       ---        3
PhD Thesis       0           ---        ---       ---        ---
MSc Thesis       5           ---        ---       ---        ---
===============  ==========  ========== ========= ========== ============            


------------

Papers
~~~~~~

------------

.. [P15] **Predictive maintenance on injection molds by generalized fault trees and anomaly detection**

- **authors**: [UA.PN]_ [UA.ER]_ [UA.JPS]_ [OLI.RA]_

- **type**: article

- **metadata**: P. Nunes, E.M. Rocha, J.P. Santos, R. Antunes (2022). Predictive maintenance on injection molds by generalized fault trees and anomaly detection.

- **status**: submitted

- **usecases**: [UC04B]_

- **url**: TBD

- **abstract**: Predictive maintenance (PdM) plays a key role in the Industry since it allows optimization of the schedule for proactive interven- tions and to take the maximum advantage of the useful lifetime of industrial assets. The reliability-centered maintenance (RCM) is based on equipment's reliability and allows the use of different maintenance strategies to optimize maintenance costs. With a recently proposed data-driven methodology entitled generalized fault trees (GFT), it is possible to assess the reliability of industrial equipment in real-time, based on their actual condition. In this paper, we exploit the GFT methodology in a completely different industrial scenario. A new training algorithm that intends to minimize operational costs, together with an anomaly detection tech- nique (isolation forest) is presented to perform the predictive maintenance of injection molds at OLI, an enterprise specialized in producing plastic parts by the injection process. The results show that the proposed methodology allows savings of 27.05% com- pared with preventive maintenance (PM) in optimized constant periods, and 63.43% compared to corrective maintenance (CM).

.. [P14] **Convolutional neural networks for identification of moving combustion chambers entering a brazing process**

- **authors**: [UA.RP]_ [UA.ER]_ [BTT.DP]_ [UA.JPS]_

- **type**: article

- **metadata**: R. Pereira, E.M. Rocha, D. Pinto, J.P. Santos (2022). Convolutional neural networks for identification of moving combustion chambers entering a brazing process.

- **status**: submitted

- **usecases**: [UC06]_

- **url**: TBD

- **abstract**: Despite the effective application of deep learning to image classification tasks, these techniques are yet to be fully implemented in the industrial environment. In particular in brazing processes, where the high temperatures destroy the majority of the identification systems, product identification is required to allow automatic product traceability. This work addresses this issue in an international company facility producing water heaters, overcoming the lack of information regarding which model of the combustion chamber is starting the brazing process. Relying on deep learning algorithms, a solution is presented to develop and implement a vision system capable of classifying, through convolutional neural networks (CNN), the 16 models of combustion chambers. The system deployed at the company’s production line, although trained with a relatively small collection of images, achieved an F1-score weighted average of 96% and 100% accuracy in classifying some of the models of combustion chambers, already implemented on the shop floor. From the building of the vision system for image acquisition until the training and deployment of the algorithms to classifying the combustion chambers are discussed.

.. [P13] **Variables influence analysis of gas leak testing using belief propagation over factor graphs**

- **authors**: [UA.JM]_ author$UA.DC [UA.ER]_

- **type**: article

- **metadata**: J. Martins, D. Costa, E.M. Rocha (2022). Variables influence analysis of gas leak testing using belief propagation over factor graphs.

- **status**: submitted

- **usecases**: [UC02]_

- **url**: TBD

- **abstract**: Leak testing provides nondestructive quality measurements and is a vital stage in the manufacturing of components that require leak-tight properties. Yet, leak tests are notorious for their sensitivity towards external and environmental factors, hampering accu- racy and reliability of test results, leading to a more costly and less efficient production cycle. The classical approach to this issue is through the use of mathematical or physical models of leakage behaviour, which are of difficult creation and low generalisation. Yet, resulting calibrations made to compensate for small deviations in testing conditions seldom consider each test-jig's unique set-up, and may still lead to unsatisfactory results. Alternatively, data-driven methods, such as the novel approach presented in this work, allow for failure analysis based solely on historical data. To achieve that, we employ a set of Variable Influence Analysis (VIA) models based on the application of the Belief Propagation algorithm to factor graphs where different events are related through correlation relationships. What differentiates our approach from other data-driven methods, such as the ones based on machine learning or deep learning methods, is the interpretability of the results and more efficient and swift implementation. We then apply VIA models to a real-world use case at Bosch Thermotechnology, centred around a differential pressure leak tester where testing frequently resulted in false rejections. Our approach is able to formally determine in a data-driven manner that, unlike initial suspicion, environmental factors show negligible impact on false rejections, and issues likely stem from equipment fault.

.. [P12] **Understanding and predicting process performance variations of a balanced manufacturing line at Bosch**

- **authors**: [UA.AB]_ [UA.ER]_ [UA.CP]_

- **type**: article

- **metadata**: A.F Brochado, E.M. Rocha, C. Pimentel (2022). Understanding and predicting process performance variations of a balanced manufacturing line at Bosch.

- **status**: accepted

- **usecases**: [UC01C]_

- **url**: TBD

- **abstract**: Industry 4.0 takes advantage of data-driven approaches to improve manufacturing processes. Root cause analysis (RCA) techniques are naturally required to support the identification of reasons for (in)efficiency processes. However, a limitation of classical RCA methods is their sensibility to data perturbations or outliers. Such sensibility phenomenon requires the implementation of robust RCA approaches. Here, methods of graph theory (queue directed graphs), operational research (multi-directional efficiency analysis), machine learning (extreme gradient boosting), and game theory (Shapley analysis) are merged together, in order to obtain a robust approach that is able to benchmark the workers acting on a discrete manufacturing process, determine the relevance level of process variables regarding a worker belonging to the (in)efficient group, and predict the worker performance variation into its next working session. A use case at Bosch ThermoTechnology is analysed to show the methodology's applicability.

.. [P11] **Benchmarking and prediction of entities performance on manufacturing processes through MEA, robust XGBoost and SHAP analysis**

- **authors**: [UA.ER]_ [UA.AB]_ [UA.BR]_

- **type**: article

- **metadata**: E.M. Rocha, A.F Brochado, B. Rato, J. Meneses (2022). Benchmarking and prediction of entities performance on manufacturing processes through MEA, robust XGBoost and SHAP analysis.

- **status**: accepted

- **usecases**: [UC01C]_

- **url**: TBD

- **abstract**: Determining the reasons for process variability of manufacturing processes is generically quite demanding. In the era of big data and Industry 4.0, data-driven root cause analysis (RCA) techniques are required to support the identification of such reasons. However, an important issue with classical RCA methods is their sensibility to data perturbations. In fact, adversarial data perturbation is currently one of the hot topics in the literature. Such sensibility phenomena requires the implementation of robust RCA approaches. Here, methods of operational research (multi-directional efficiency analysis), machine learning (extreme gradient boosting), and game theory (Shapley values) are merged, in order to obtain a robust approach that is able to benchmark the entities acting on a manufacturing process, determine the importance level of process variables regarding an entity belonging to the (in)efficient group, and predict the entity performance of its next working session. A use case in a Portuguese leader company that manufactures porcelain tableware, high-quality glass and crystal, is analysed to show the methodology's success.

.. [P10] **Impact analysis of KPI scenarios, automated best practices identification, and deviations on manufacturing processes**

- **authors**: [BTT.MJL]_ [UA.ER]_

- **type**: proceedings or special issue

- **metadata**: M.J. Lopes, E.M. Rocha (2022). Impact analysis of KPI scenarios, automated best practices identification, and deviations on manufacturing processes. Accepted on ETFA 2022 is the 27th Annual Conference of the IEEE Industrial Electronics Society (IES).

- **status**: accepted

- **usecases**: [UC07]_

- **url**: TBD

- **abstract**: Data-driven applications are becoming more and more ubiquitous throughout the manufacturing industry. The decision of which projects to start often comes from the input of process experts who identify a concrete potential for improvement in a certain area. However, a different approach may be taken when it is not entirely clear where to start looking for patterns or potential information which can trigger continuous improvement activities. This is specially relevant in manufacturing processes with a high level of maturity and stability. In this article, the authors propose a generic approach for conducting impact analysis with a use case which aims to deconstruct the Overall Equipment Effectiveness (OEE), a quite known Key Performance Indicator (KPI), in a manufacturing production line of a Bosch plant located in Portugal. This methodology is focused on identifying the best and worst scenarios by creating a ranking and subsequently pinpointing possible causes, identifying best practices and devising strategies to deal with these non-optimal scenarios. The methodology can be seen as an alternative to complex statistical hypothesis testing by relying on measuring several distribution differences.

.. [P09] **Minimizing false-rejection rates in gas leak testing using an ensemble multiclass classifier for unbalanced data**

- **authors**: [UA.DC]_ [UA.ER]_ [BTT.PR]_

- **type**: article

- **metadata**: D. Costa, E.M. Rocha, P. Ramalho (2022). Minimizing false-rejection rates in gas leak testing using an ensemble multiclass classifier for unbalanced data.

- **status**: accepted

- **usecases**: [UC02]_

- **url**: TBD

- **abstract**: Leak testing is a fundamental stage in the manufacturing process of leak-tight products providing a non-destructive quality measurement. However, leak tests are often influenced by environmental and external factors that increase the risk of test inaccuracy. These phenom- ena are difficult to properly account for through mathematical models, as they are particular to each individual testing setup, nonetheless, they signify great expense to manufacturers due to substantial bottlenecking. In this paper, we introduce a real-world use-case at Bosch Thermotechnology facilities where over 23.98% of testing instances result in false-rejections. We then propose a data-driven, equipment agnostic, pro- cedure for leak testing fault classification. We first identify seven relevant classes for fault diagnosis, and, due to the highly unbalanced nature of these classes (minority class represents only 0.27% of all data), we apply a novel unbalanced multiclass classification pipeline based on an ensemble of heterogeneous classifiers. Through this method, we are able to obtain F1-macro of 90%, representing a performance improvement of over 120% in comparison with conventional Auto-ML approaches.

.. [P08] **Real-time condition-based maintenance of friction welding tools by generalized fault trees**

- **authors**: [UA.PN]_ [UA.ER]_ [BTT.JN]_ [UA.JPS]_

- **type**: article

- **metadata**: P. Nunes, E.M. Rocha, J. Neves, J.P. Santos (2022). Real-time condition-based maintenance of friction welding tools by generalized fault trees.

- **status**: accepted

- **usecases**: [UC05]_

- **url**: TBD

- **abstract**: In manufacturing processes, root cause analysis of tools' failures is crucial to determine the system reliability and to derive cost minimizing strategies. Condition-based maintenance (CBM) is one of the relevant policies to reduce costs of tools usage subjected to degradation processes. In a previous work, the authors introduced a new statistical methodology entitled Generalized Fault Tree (GFT) analysis that demonstrated good results for reliability analysis and root cause analysis. In this work, we propose a new dynamic CBM methodology, through real-time failure forecasting of the replacement instant of friction welding tools at a Bosch TermoTechnology facility, based on the dynamic update of the GFT root probability. The GFT structure is described by data-driven basic events (BEs), obtained from an embedded accelerometer, and the tree that best describes the tools' failures is obtained through a new training process that employs a pruning technique to reduce computational complexity. The results show that we can reduce at least 12% of the costs per welding cycle by applying the GFT approach and CBM, when compared with the current policy of preventive maintenance in (optimized) constant cycles replacement periods.

.. [P07] **Reliability analysis of sensorized stamping presses by generalized fault trees**

- **authors**: [UA.ER]_ [UA.PN]_ [UA.JPS]_

- **type**: proceedings or special issue

- **metadata**: E.M. Rocha, P. Nunes, J.P. Santos (2022). Reliability analysis of sensorized stamping presses by generalized fault trees. Proceedings of the International Conference on Industrial Engineering and Operations Management, Istanbul, Turkey, March 7-10.

- **status**: accepted

- **usecases**: [UC01A]_

- **url**: TBD

- **abstract**: Reliability analysis and probability methods are of extreme importance for understanding the behavior of critical systems. In this scope, dynamic fault trees (DFTs) are consolidated graphical models, previously applied at known entities, such as NASA and TESLA. However, the (classical) DFTs analysis has a known issue; the fact that it assumes that the distribution of basic events (BEs) follows the exponential/Weibull distribution, which is often a rough approximation of real data distributions. Moreover, building a DFT model for a real system requires specialized knowledge, to infer the root causes of failures. In this work, a new formalism for the analysis of so-called generic fault trees (GFTs) is proposed which extends DFT to data-driven scenarios, based on the notion of h-approximation of the associated distributions, where leaves may contain an arbitrary compact support distribution or an h-truncation of a distribution. The approach is validated against known solutions in the literature, showing great accuracy. An optimization process is employed to generate the best structure for training a GFT that fits a given data. A real use case scenario of a stamping press of Bosch ThermoTechnology is presented, to prove the capabilities of the proposed approach in the context of Industry 4.0.

.. [P06] **PDCA protocol to ensure a data-driven approach for problem-solving**

- **authors**: [UA.AB]_ [UA.ER]_ [UA.CP]_

- **type**: proceedings or special issue

- **metadata**: A.F. Brochado, E.M. Rocha, C. Pimentel (2022). PDCA protocol to ensure a data-driven approach for problem-solving. Proceedings of the International Conference on Industrial Engineering and Operations Management, Istanbul, Turkey, March 7-10.

- **status**: accepted

- **usecases**: [UC07]_

- **url**: TBD

- **abstract**: Problem-solving based, as much as possible, on real data, expertise knowledge, and field observation is a quite desired objective, but creates several difficulties on deployment in real situations. In this work, a data-driven version of the well-known PDCA cycle is proposed for the continuous improvement of a general class of problems that can be represented by key performance indicators (KPI). Such class is wide enough to accommodate several real problems, but still has a controlled level of complexity that allows to define a general data-driven protocol covering all the (sub)steps of the cycle. New approaches and alternatives in the literature are discussed. A brief example of one of the steps of the protocol is clarified with real data from a company adopting many of the new Industry 4.0 technologies.

.. [P05] **Predictive maintenance on sensorized stamping presses by time series segmentation, anomaly detection, and classification algorithms**

- **authors**: [UA.DCL]_ [UA.DC]_ [UA.ER]_ [BTT.DA]_ [UA.JPS]_

- **type**: article

- **metadata**: D. Coelho, D. Costa, E.M. Rocha, D. Almeida, J.P. Santos (2022). Predictive maintenance on sensorized stamping presses by time series segmentation, anomaly detection, and classification algorithms. Procedia Computer Science 200:1184-1193.

- **status**: published

- **usecases**: [UC01A]_

- **url**: `https://www.sciencedirect.com <https://www.sciencedirect.com/science/article/pii/S1877050922003271>`_

- **abstract**: Sheet metal forming tools, like stamping presses, play an ubiquitous role in the manufacture of several products. With increasing requirements of quality and efficiency, ensuring maximum uptime of these tools is fundamental to marketplace competitiveness. Using anomaly detection and predictive maintenance techniques, it is possible to develop lower risk and more intelligent approaches to maintenance scheduling, however, industrial implementations of these methods remain scarce due to the difficulties of obtaining acceptable results in real-world scenarios, making applications of such techniques in stamping processes seldom found. In this work, we propose a combination of two distinct approaches: (a) time segmentation together with feature dimension reduction and anomaly detection; and (b) machine learning classification algorithms, for effective downtime prediction. The approach (a)+(b) allows for an improvement rate up to 22.971% of the macro F1-score, when compared to sole approach (b). A ROC AUC index of 96% is attained by using Randomized Decision Trees, being the best classifier of twelve tested. An use case with a decentralized predictive maintenance architecture for the downtime forecasting of a stamping press, which is a critical machine in the manufacturing facilities of Bosch Thermo Technology, is discussed.

.. [P04] **Bottleneck prediction and data-driven discrete-event simulation for a balanced manufacturing line**

- **authors**: [UA.ER]_ [BTT.MJL]_

- **type**: article

- **metadata**: E.M. Rocha, M.J. Lopes (2022). Bottleneck prediction and data-driven discrete-event simulation for a balanced manufacturing line. Procedia Computer Science 200:1145-1154.

- **status**: published

- **usecases**: [UC01C]_

- **url**: `https://www.sciencedirect.com <https://www.sciencedirect.com/science/article/pii/S1877050922003234>`_

- **abstract**: Bottleneck identification is a relevant tool for continuous optimization of production lines. In this work, we implement a data-driven discrete-event simulator (DDS) based on experimental distributions, obtained from real historical data. The DDS allows to analyse the behavior of a balanced manufacturing line at Bosch Thermotechnology, under different hypotheses. It shows that some scenarios perceived as likely to increase output may actually decrease production metrics, reveals the importance of line injection rates, and leads to the need for adequate real time bottleneck forecasting tools, which allow shift managers intervention in a useful time frame. Eleven prediction models are tested, where a random forest and a multi-layer perceptron attain the best performances (above 95% in all metrics). This data flow is operationalized through a micro-services pipeline which is briefly discussed.


.. image:: ../portfolios/AH2020.PPS2.P04.png
   :alt: Portfolio
   :width: 800
   :target: https://gitlab.com/sDLsystem/ah2020pps2-docs/-/raw/master/docs/portfolios/AH2020.PPS2.P04.pdf?inline=false

.. [P03] **Worker's benchmark using MEA for throughput improvement in a manufacturing production system**

- **authors**: [UA.ER]_ [UA.AB]_ [UA.AM]_

- **type**: article

- **metadata**: E.M. Rocha, A.F. Brochado, A. Moura (2022). Worker's benchmark using MEA for throughput improvement in a manufacturing production system. Procedia Computer Science 200:1451-1460.

- **status**: published

- **usecases**: [UC01C]_ [UC07]_

- **url**: `https://www.sciencedirect.com <https://www.sciencedirect.com/science/article/pii/S1877050922003556>`_

- **abstract**: The human factor plays a relevant role in all manual or partially automatic production systems, specially, the ones showing reliable and balanced dynamics. In the literature, parametric or survey-based models are quite common for performance evaluation of production workers. In this work, multi-directional efficiency analysis is used instead, for root cause analysis of product reworks and bottlenecks occurrence, according to four worker-related parameters: experience time, wage, delay time and response time. The approach allows to identify individual inefficiencies per tuple worker/working shift and to cluster them according to similar inefficiency parameters. In addition, this work opens a path to new applications of multi-directional efficiency analysis to problems in the manufacturing industry.

.. [P02] **A data-driven model with minimal information for bottleneck detection - application at Bosch Thermotechnology**

- **authors**: [UA.AB]_ [UA.ER]_ [BTT.DA]_ [UA.AS]_ [UA.AM]_

- **type**: article

- **metadata**: A.F. Brochado, E.M. Rocha, D. Almeida, A. de Sousa, A. Moura (2022). A Data-Driven Model with Minimal Information for Bottleneck Detection - Application at Bosch Thermotechnology.

- **status**: accepted

- **usecases**: [UC01C]_

- **url**: TBD

- **abstract**: A data-driven model based on minimal information retrieved from a manufacturing execution system is explored for bottleneck detection. Minimal information (MI) is meant as the most elementary information from production line operations, enough to autonomously generate the abstract manufacturing layout, to detect and predict bottlenecks. The results show that the timestamps when each product exits a location are the MI. In the literature, most common data-driven approaches employ data from diverse production variables (machine processing times, machine state tags, input timestamps, etc.) for a detailed analysis of bottlenecks. However, for companies initiating their digitalization process (requiring the smallest hardware investment), a bottom-top approach is still missing. To this extent, a general abstract model of a production line is proposed, named queue directed graph (QDG). Incorporating the MI, the QDG model is able to represent a job-shop with a dis- crete production environment and to calculate production metrics. This work has been employed in the production system of a Bosch factory, in Portugal, using their manufacturing datasets for validation. Variants of two well-known bottleneck detection methods were adapted to Bosch's use case and implemented: the Active Period Method (APM) and the Average Active Period Method (AAPM).

.. [P01] **General model for metrics calculation and behavior prediction in manufacturing industry**

- **authors**: [BTT.MJL]_, [UA.ER]_, [UA.PG]_, [BTT.NF]_

- **type**: book chapter

- **metadata**: M.J. Lopes, E.M. Rocha, P. Georgieva, N. Ferreira (2021). General Model for Metrics Calculation and Behavior Prediction in Manufacturing Industry, Chapter of the Handbook of Research on Applied Data Science and Artificial Intelligence in Business and Industry, V. Chkoniya (ed.), vol. 2, 263-290, IGI-Global.

- **status**: published

- **usecases**: [UC01C]_

- **url**: `https://www.igi-global.com <https://www.igi-global.com/chapter/general-model-for-metrics-calculation-and-behavior-prediction-in-the-manufacturing-industry/284984>`_

- **abstract**: In this chapter, a comprehensive description of a generic framework aimed at solving various predictive data-driven related use cases, occurring in the manufacturing industry, is provided. The framework is rooted in a general mathematical model so called queue directed graph (QDG). With the aid of QDGs and containerized microservices implementations, the typology of the system is analyzed, and real use cases are explained. The goal is for this framework to be able to be used with all use cases which fit in this typology. As an example, a data generation distribution model is proposed, the parameter stability and predictive robustness are studied, and automated machine learning approaches are discussed to predict the throughput time of products in a manufacturing production line just by knowing the processing time in their first stations.

------------

Reports
~~~~~~~

------------

.. [R03] **Root cause analysis by generalized fault trees**

- **authors**: [PN]_ [UA.ER]_ [UA.JPS]_

- **location**: Encontro Ciência, Lisboa, Portugal, 18 de maio 2022.

- **format**: poster

- **usecases**: [UC01B]_ [UC05]_

.. [R02] **Construction of causal networks models of assembly lines using mutual information**

- **authors**: [UA.FB]_ [UA.GB]_

- **location**: International School and Conference on Network Science (NetSciX2022), Porto

- **format**: poster

- **usecases**: [UC03]_

.. [R01] **Data exploratory analysis of a gas leakage process at Bosch TT**

- **authors**: [FCUP.FR]_ [FCUP.ID]_

- **format**: notebook

- **usecases**: [UC02]_

------------

Thesis
~~~~~~

------------

.. [T05] **[PT] Sistema de identificação de câmaras de combustão com recurso a machine learning**

- **type**: MSc thesis

- **metadata**: [UA.RP]_, Sistema de identificação de câmaras de combustão com recurso a machine learning. Supervised by [UA.ER]_, [UA.JPS]_, MSc Eng. Mecânica, UA, 2022.

- **status**: submitted

- **usecases**: [UC06]_

- **url**: TBD

.. [T04] **[PT] Proposta de um sistema de IoT para recolha e processamento de dados**

- **type**: MSc thesis

- **metadata**: [UA.BM]_, Proposta de um sistema de IoT para recolha e processamento de dados. Supervised by [UA.JPS]_, [UA.ER]_, MSc Eng. Mecânica, UA, 2022.

- **status**: submitted

- **usecases**: [UC04A]_ [UC04B]_

- **url**: TBD

.. [T03] **Development of a recommendation system in PCBA repair**

- **type**: MSc thesis

- **metadata**: [UA.JPC]_, Development of a recommendation system in PCBA repair. Supervised by [UA.ER]_, [UA.JPS]_, MSc Eng. Mecânica, UA, 2022.

- **status**: submitted

- **usecases**: usace$UC03

- **url**: TBD

.. [T02] **[PT] Uma abordagem de manutenção preditiva baseada na segmentação de séries temporais**

- **type**: MSc thesis

- **metadata**: [UA.DCL]_, Uma abordagem de manutenção preditiva baseada na segmentação de séries temporais. Supervised by [UA.ER]_, [UA.JPS]_, MSc Eng. Mecânica, UA, 2021.

- **status**: concluded

- **usecases**: [UC01A]_

- **url**: `https://ria.ua.pt <https://ria.ua.pt/handle/10773/32120>`_

.. [T01] **[PT] Controlo de qualidade de PCBA, classificação de erros e recomendação de reparação**

- **type**: MSc thesis

- **metadata**: [UA.MP]_, Controlo de qualidade de PCBA, classificação de erros e recomendação de reparação. Supervised by [UA.ER]_, MSc Matemática e Aplicações, UA, 2021.

- **status**: concluded

- **usecases**: [UC03]_

- **url**: `https://ria.ua.pt <https://ria.ua.pt/handle/10773/32748>`_

------------

Communications
~~~~~~~~~~~~~~

------------

.. [C10] **Classifying false-rejections of manufacturing processes - a multiclass classification approach for rejection analysis in unbalanced manufacturing data**

- **presenter**: [UA.DC]_

- **location**: [Best Presentation] 5th International Conference on Technologies for the Wellbeing and Sustainable Manufacturing Solutions (TEchMA2022), Aveiro, 2022

- **source_output**: [P09]_

.. [C09] **Generalized fault trees - a data-driven methodology for reliability analysis**

- **presenter**: [UA.PN]_

- **location**: 5th International Conference on Technologies for the Wellbeing and Sustainable Manufacturing Solutions (TEchMA2022), Aveiro, 2022

- **source_output**: [P08]_

.. [C08] **Reliability analysis of sensorized stamping presses by generalized fault trees**

- **presenter**: [UA.PN]_

- **location**: Conference on Industrial Engineering and Operations Management, Istanbul, 2022

- **source_output**: [P07]_

.. [C07] **Development of embedded maintenance algorithms for equipment failure forecast**

- **presenter**: [UA.DC]_

- **location**: Research Summit, Universidade de Aveiro, 2021

- **source_output**: [P05]_

.. [C06] **General model for metrics calculation and behavior prediction in manufacturing industry**

- **presenter**: [BTT.MJL]_

- **location**: Research Summit, Universidade de Aveiro, 2021

- **source_output**: [P01]_

.. [C05] **General model for metrics calculation and behavior prediction in manufacturing industry**

- **presenter**: [BTT.MJL]_

- **location**: Research Summit, Universidade de Aveiro, 2021

- **source_output**: [P01]_

.. [C04] **Bottleneck identification and prediction in manufacturing systems**

- **presenter**: [UA.AB]_

- **location**: [Prize for best pitch on EGI] Research Summit, Universidade de Aveiro, 2021

- **source_output**: [P02]_

.. [C03] **Worker's benchmark using MEA for throughput improvement in a manufacturing production system**

- **presenter**: [UA.AB]_

- **location**: 3rd International Conference on Industry 4.0 and Smart Manufacturing - ISM 2021 Upper Austria University of Applied Sciences - Hagenberg Campus - Linz, Austria Blended Conference, 17-19 November 2021

- **source_output**: [P03]_

.. [C02] **Predictive maintenance on sensorized stamping presses by time series segmentation, anomaly detection, and classification algorithms**

- **presenter**: [UA.ER]_

- **location**: 3rd International Conference on Industry 4.0 and Smart Manufacturing - ISM 2021 Upper Austria University of Applied Sciences - Hagenberg Campus - Linz, Austria Blended Conference, 17-19 November 2021

- **source_output**: [P05]_

.. [C01] **Bottleneck prediction and data-driven discrete-event simulation or a balanced manufacturing line**

- **presenter**: [UA.ER]_

- **location**: 3rd International Conference on Industry 4.0 and Smart Manufacturing - ISM 2021 Upper Austria University of Applied Sciences - Hagenberg Campus - Linz, Austria Blended Conference, 17-19 November 2021

- **source_output**: [P04]_

