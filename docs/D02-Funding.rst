D02-Funding
===========

------------

POCI2020
~~~~~~~~

------------

- **ref**: POCI-01-0247-FEDER-046103 LISBOA-01-0247-FEDER-046103

- **languages**: call/EN output/EN

- **measures**: P2020; SI I&DT - Programas Mobilizadores

- **eligible_costs**: 8 184 504,14€

- **funding**:

 - FEDER/COMPETE2020: 4 823 523,11 €

 - FEDER/LISBOA2020: 375 856,02 €

